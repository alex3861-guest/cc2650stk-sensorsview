/*
This module is a part of cc2650stk-sensorsview programm.
cc2650stk-sensorsview is a small app to read all available
sensors from TI cc2650stk sensortag device with default firmware.

Copyright (C) <2018>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "widget_sens_view.h"
#include "ui_widget.h"
#include <math.h>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    oBlutoothDevDiscAgent = new QBluetoothDeviceDiscoveryAgent();
    connect(oBlutoothDevDiscAgent, SIGNAL(deviceDiscovered(const QBluetoothDeviceInfo&)),
            this, SLOT(fAddDevice(const QBluetoothDeviceInfo&)));
    connect(oBlutoothDevDiscAgent, SIGNAL(error(QBluetoothDeviceDiscoveryAgent::Error)),
            this, SLOT(fDeviceScanError(QBluetoothDeviceDiscoveryAgent::Error)));
    connect(oBlutoothDevDiscAgent, SIGNAL(finished()), this, SLOT(fScanFinished()));
    aStartNotifications = QByteArray::fromHex("0100");
    aStopNotifications = QByteArray::fromHex("0000");
    aStartMeasurement = QByteArray::fromHex("01");
    aStopMeasurement = QByteArray::fromHex("00");
    ui->lbStatus->setText("Status Line");
    ui->lbStatus->adjustSize();
}

void Widget::fAddDevice(const QBluetoothDeviceInfo &device)
{
    if (device.coreConfigurations() & QBluetoothDeviceInfo::LowEnergyCoreConfiguration)
    {
        stDevice vDevInfo;
        vDevInfo.name = device.name();
        vDevInfo.address = device.address();
        lDevList.append(vDevInfo);
        QString vS;
        vS = vDevInfo.name + " - " + vDevInfo.address.toString();
        ui->cbBLEdevices->addItem(vS);
    }
}

void Widget::fDeviceScanError(QBluetoothDeviceDiscoveryAgent::Error error)
{
    QString vS;
    switch (error)
    {
    case QBluetoothDeviceDiscoveryAgent::NoError: vS = "No error.";
        break;
    case QBluetoothDeviceDiscoveryAgent::PoweredOffError: vS = "PoweredOffError!";
        break;
    case QBluetoothDeviceDiscoveryAgent::InputOutputError: vS = "I/O error!";
        break;
    case QBluetoothDeviceDiscoveryAgent::InvalidBluetoothAdapterError: vS = "invalid bt adapter error!";
        break;
    case QBluetoothDeviceDiscoveryAgent::UnsupportedPlatformError: vS = "Unsupported platform error!";
        break;
    case QBluetoothDeviceDiscoveryAgent::UnknownError: vS = "Unknown error!";
        break;
    default: vS = "unknown error!";
        break;
    }
    ui->lbStatus->setText(vS);
    ui->lbStatus->adjustSize();
}

void Widget::fScanFinished()
{
    ui->lbStatus->setText("Scanning Finished");
    ui->lbStatus->adjustSize();
}
//======================================================================================

Widget::~Widget()
{
    delete ui;
}

void Widget::on_btBluetoothScan_clicked()
{
    lDevList.clear();
    ui->cbBLEdevices->clear();
    oBlutoothDevDiscAgent->start();
    if (oBlutoothDevDiscAgent->isActive())
        ui->lbStatus->setText("Scaning in progress.");
    else
        ui->lbStatus->setText("Scaning not active.");
    ui->lbStatus->adjustSize();
}


//========= Sevrices discovery procedures ================================

void Widget::on_btConnectToDevice_clicked()
{
    if (ui->cbBLEdevices->count() > 0)
    {
        lServicesList.clear();
//        ui->cbServices->clear();
        int vInd = ui->cbBLEdevices->currentIndex();
        oBleController = new QLowEnergyController(lDevList[vInd].address, this);
        connect(oBleController, SIGNAL(serviceDiscovered(QBluetoothUuid)),
                this, SLOT(fServiceDiscovered(QBluetoothUuid)));
        connect(oBleController, SIGNAL(discoveryFinished()),
                this, SLOT(fServiceScanFinished()));
        connect(oBleController, SIGNAL(error(QLowEnergyController::Error)),
                this, SLOT(fControllerError(QLowEnergyController::Error)));
        connect(oBleController, SIGNAL(connected()),
                this, SLOT(fDeviceConnected()));
        connect(oBleController, SIGNAL(disconnected()),
                this, SLOT(fDeviceDisconnected()));
        oBleController->connectToDevice();
        ui->lbStatus->setText("attempting to connect...");
    }
    else
        ui->lbStatus->setText("Error!!! There is no one device in the list.");
    ui->lbStatus->adjustSize();
}

void Widget::fDeviceConnected()
{
    oBleController->discoverServices();
    ui->lbStatus->setText("device connected");
    ui->lbStatus->adjustSize();
}

void Widget::fServiceDiscovered(QBluetoothUuid vGatt)
{
    QString vS;
    vS = vGatt.toString();
    lServicesList.append(vGatt);
//    ui->cbServices->addItem(vS);
    ui->lbStatus->setText("Service discovered");
    ui->lbStatus->adjustSize();
}

void Widget::fServiceScanFinished()
{
    ui->lbStatus->setText("services discovery finished");
    ui->lbStatus->adjustSize();
    fConnectToServices();
}

void Widget::fControllerError(QLowEnergyController::Error error)
{
    switch (error)
    {
        case QLowEnergyController::NoError: ui->lbStatus->setText("No error.");
            break;
        case QLowEnergyController::UnknownError: ui->lbStatus->setText("Unknown error");
            break;
        case QLowEnergyController::UnknownRemoteDeviceError: ui->lbStatus->setText("Unknown Remote device error");
            break;
        case QLowEnergyController::NetworkError: ui->lbStatus->setText("Network error");
            break;
        case QLowEnergyController::InvalidBluetoothAdapterError: ui->lbStatus->setText("Invalid bluetooth adapter error");
            break;
        case QLowEnergyController::ConnectionError: ui->lbStatus->setText("Connection error");
            break;
//        case QLowEnergyController::AdvertisingError: ui->lbStatus->setText("Advertising error");
//            break;
        default: ui->lbStatus->setText("Unknown error!!");
            break;
    }
    ui->lbStatus->adjustSize();
}

void Widget::fDeviceDisconnected()
{
    ui->lbStatus->setText("Device disconnected.");
    ui->lbStatus->adjustSize();
}


void Widget::fConnectToServices()
{
    if (lServicesList.count() > 0)
    {
        //Temperature service---------------------------------------------------
        oBleServiceTemp = oBleController->createServiceObject(lServicesList[2]);
        connect(oBleServiceTemp, SIGNAL(stateChanged(QLowEnergyService::ServiceState)),
                this, SLOT(fServiceStateChangedTemp(QLowEnergyService::ServiceState)));
        connect(oBleServiceTemp, SIGNAL(error(QLowEnergyService::ServiceError)),
                this, SLOT(fServiceError(QLowEnergyService::ServiceError)));
        connect(oBleServiceTemp, SIGNAL(characteristicChanged(QLowEnergyCharacteristic,QByteArray)),
                this, SLOT(fCharacteristicReadTemp(QLowEnergyCharacteristic,QByteArray)));

        //Humidity service------------------------------------------------------
        oBleServiceHum = oBleController->createServiceObject(lServicesList[3]);
        connect(oBleServiceHum, SIGNAL(stateChanged(QLowEnergyService::ServiceState)),
                this, SLOT(fServiceStateChangedHum(QLowEnergyService::ServiceState)));
        connect(oBleServiceHum, SIGNAL(error(QLowEnergyService::ServiceError)),
                this, SLOT(fServiceError(QLowEnergyService::ServiceError)));
        connect(oBleServiceHum, SIGNAL(characteristicChanged(QLowEnergyCharacteristic,QByteArray)),
                this, SLOT(fCharacteristicReadHum(QLowEnergyCharacteristic,QByteArray)));

        //Barometer service-----------------------------------------------------
        oBleServiceBar = oBleController->createServiceObject(lServicesList[4]);
        connect(oBleServiceBar, SIGNAL(stateChanged(QLowEnergyService::ServiceState)),
                this, SLOT(fServiceStateChangedBar(QLowEnergyService::ServiceState)));
        connect(oBleServiceBar, SIGNAL(error(QLowEnergyService::ServiceError)),
                this, SLOT(fServiceError(QLowEnergyService::ServiceError)));
        connect(oBleServiceBar, SIGNAL(characteristicChanged(QLowEnergyCharacteristic,QByteArray)),
                this, SLOT(fCharacteristicReadBar(QLowEnergyCharacteristic,QByteArray)));

        //Movement service------------------------------------------------------
        oBleServiceMov = oBleController->createServiceObject(lServicesList[5]);
        connect(oBleServiceMov, SIGNAL(stateChanged(QLowEnergyService::ServiceState)),
                this, SLOT(fServiceStateChangedMov(QLowEnergyService::ServiceState)));
        connect(oBleServiceMov, SIGNAL(error(QLowEnergyService::ServiceError)),
                this, SLOT(fServiceError(QLowEnergyService::ServiceError)));
        connect(oBleServiceMov, SIGNAL(characteristicChanged(QLowEnergyCharacteristic,QByteArray)),
                this, SLOT(fCharacteristicReadMov(QLowEnergyCharacteristic,QByteArray)));

        //Luxometer sirvice-----------------------------------------------------
        oBleServiceLux = oBleController->createServiceObject(lServicesList[6]);
        connect(oBleServiceLux, SIGNAL(stateChanged(QLowEnergyService::ServiceState)),
                this, SLOT(fServiceStateChangedLux(QLowEnergyService::ServiceState)));
        connect(oBleServiceLux, SIGNAL(error(QLowEnergyService::ServiceError)),
                this, SLOT(fServiceError(QLowEnergyService::ServiceError)));
        connect(oBleServiceLux, SIGNAL(characteristicChanged(QLowEnergyCharacteristic,QByteArray)),
                this, SLOT(fCharacteristicReadLux(QLowEnergyCharacteristic,QByteArray)));

        oBleServiceTemp->discoverDetails();
        oBleServiceHum->discoverDetails();
        oBleServiceBar->discoverDetails();
        oBleServiceMov->discoverDetails();
        oBleServiceLux->discoverDetails();

        ui->lbStatus->setText("discovering details");
        ui->lbStatus->adjustSize();
    }
}

void Widget::fServiceStateChangedTemp(QLowEnergyService::ServiceState vState)
{
    switch (vState)
    {
        case QLowEnergyService::ServiceDiscovered:
        {
            lCharacteristicsTemp.clear();
            lCharacteristicsTemp = oBleServiceTemp->characteristics();
            ui->lbStatus->setText("Temperature Characteristics discovered.");
            ui->lbStatus->adjustSize();
            vDescriptorTemp = lCharacteristicsTemp[0].descriptor(QBluetoothUuid::ClientCharacteristicConfiguration);
            //enable notification
            oBleServiceTemp->writeDescriptor(vDescriptorTemp, aStartNotifications);
            oBleServiceTemp->writeCharacteristic(lCharacteristicsTemp[1], aStartMeasurement);
        }
            break;
        default:
            break;
    }
}

void Widget::fServiceStateChangedHum(QLowEnergyService::ServiceState vState)
{
    switch (vState)
    {
        case QLowEnergyService::ServiceDiscovered:
        {
        lCharacteristicsHum.clear();
        lCharacteristicsHum = oBleServiceHum->characteristics();
        ui->lbStatus->setText("Humidity Characteristics discovered.");
        ui->lbStatus->adjustSize();
        vDescriptorHum = lCharacteristicsHum[0].descriptor(QBluetoothUuid::ClientCharacteristicConfiguration);
        //enable notification
        oBleServiceHum->writeDescriptor(vDescriptorHum, aStartNotifications);
        oBleServiceHum->writeCharacteristic(lCharacteristicsHum[1], aStartMeasurement);
        }
            break;
        default:
            break;
    }
}
void Widget::fServiceStateChangedBar(QLowEnergyService::ServiceState vState)
{
    switch (vState)
    {
        case QLowEnergyService::ServiceDiscovered:
        {
        lCharacteristicsBar.clear();
        lCharacteristicsBar = oBleServiceBar->characteristics();
        ui->lbStatus->setText("Borometer Characteristics discovered.");
        ui->lbStatus->adjustSize();
        vDescriptorBar = lCharacteristicsBar[0].descriptor(QBluetoothUuid::ClientCharacteristicConfiguration);
        //enable notification
        oBleServiceBar->writeDescriptor(vDescriptorBar, aStartNotifications);
        oBleServiceBar->writeCharacteristic(lCharacteristicsBar[1], aStartMeasurement);
        }
            break;
        default:
            break;
    }
}

void Widget::fServiceStateChangedMov(QLowEnergyService::ServiceState vState)
{
    switch (vState)
    {
        case QLowEnergyService::ServiceDiscovered:
        {
        lCharacteristicsMov.clear();
        lCharacteristicsMov = oBleServiceMov->characteristics();
        ui->lbStatus->setText("Movement Characteristics discovered.");
        ui->lbStatus->adjustSize();
        vDescriptorMov = lCharacteristicsMov[0].descriptor(QBluetoothUuid::ClientCharacteristicConfiguration);
        //enable notification
        oBleServiceMov->writeDescriptor(vDescriptorMov, aStartNotifications);
        oBleServiceMov->writeCharacteristic(lCharacteristicsMov[1], QByteArray::fromHex("7F00"));
        }
            break;
        default:
            break;
    }
}

void Widget::fServiceStateChangedLux(QLowEnergyService::ServiceState vState)
{
    switch (vState)
    {
        case QLowEnergyService::ServiceDiscovered:
        {
        lCharacteristicsLux.clear();
        lCharacteristicsLux = oBleServiceLux->characteristics();
        ui->lbStatus->setText("Luxometer Characteristics discovered.");
        ui->lbStatus->adjustSize();
        vDescriptorLux = lCharacteristicsLux[0].descriptor(QBluetoothUuid::ClientCharacteristicConfiguration);
        //enable notification
        oBleServiceLux->writeDescriptor(vDescriptorLux, aStartNotifications);
        oBleServiceLux->writeCharacteristic(lCharacteristicsLux[1], aStartMeasurement);
        }
            break;
        default:
            break;
    }
}

//void Widget::fCharacteristicChanged(QLowEnergyCharacteristic, QByteArray)
//{

//}

//void Widget::fConfirmedDescriptorWrite(QLowEnergyDescriptor, QByteArray)
//{

//}

void Widget::fServiceError(QLowEnergyService::ServiceError error)
{
    switch (error)
    {
        case QLowEnergyService::NoError: ui->lbStatus->setText("No error.");
            break;
        case QLowEnergyService::CharacteristicReadError: ui->lbStatus->setText("Characteristic Read error!");
            break;
        case QLowEnergyService::OperationError: ui->lbStatus->setText("Operation error!");
            break;
        case QLowEnergyService::CharacteristicWriteError: ui->lbStatus->setText("Characteristic Write error!");
            break;
        case QLowEnergyService::DescriptorReadError: ui->lbStatus->setText("Description Read error!");
            break;
        case QLowEnergyService::DescriptorWriteError: ui->lbStatus->setText("Description Write error!");
            break;
        case QLowEnergyService::UnknownError: ui->lbStatus->setText("Unknown error!");
            break;
        default:
            break;
    }
    ui->lbStatus->adjustSize();
}

void Widget::fCharacteristicReadTemp(QLowEnergyCharacteristic blechar, QByteArray aValue)
{
    QString vS, vS1;
    vLSB = aValue[0];
    vMSB = aValue[1];
    vValue = vLSB + (vMSB << 8);
    vValue >>= 2;
    vFValue = ((float)(vValue)) * vScaleLSB;
    vS1 = vS.setNum(vFValue);
    ui->lbObjTemp->setText(vS1);
    vLSB = aValue[2];
    vMSB = aValue[3];
    vValue = vLSB + (vMSB << 8);
    vValue >>= 2;
    vFValue = ((float)(vValue)) * vScaleLSB;
    vS1 = vS.setNum(vFValue);
    ui->lbAmbTemp->setText(vS1);
}

void Widget::fCharacteristicReadHum(QLowEnergyCharacteristic blechar, QByteArray aValue)
{
    QString vS, vS1;
    uint16_t value;
    vLSB = aValue[0];
    vMSB = aValue[1];
    value = vLSB + (vMSB << 8);
    vFValue = ((float)(value)/65536) * 165 - 40;
    vS1 = vS.setNum(vFValue);
    ui->lbHumTemp->setText(vS1);
    vLSB = aValue[2];
    vMSB = aValue[3];
    value = vLSB + (vMSB << 8);
    vFValue = ((float)(value)/65536) * 100;
    vS1 = vS.setNum(vFValue);
    ui->lbHumidity->setText(vS1);
}

void Widget::fCharacteristicReadBar(QLowEnergyCharacteristic blechar, QByteArray aValue)
{
    QString vS, vS1;
    uint32_t vValue = aValue[0] + (aValue[1] << 8) + (aValue[2] << 16);
    vFValue = vValue / 100.0f;
    vS1 = vS.setNum(vFValue);
    ui->lbBarTemp->setText(vS1);
//    uint8_t vlsb, vmsb, vext;
    vLSB = aValue[3];
    vMSB = aValue[4];
    vEXT = aValue[5];
    vValue = vLSB + (vMSB << 8) + (vEXT << 16);
    vFValue = vValue / 100.0f;
    vS1 = vS.setNum(vFValue);
    ui->lbBarPressure->setText(vS1);
}

void Widget::fCharacteristicReadMov(QLowEnergyCharacteristic blechar, QByteArray aValue)
{
    QString vS, vS1;
    vLSB = aValue[0];
    vMSB = aValue[1];
    vValue = vLSB + (vMSB << 8);
    vFValue = (vValue * 1.0)/(32968/2);
    vS1 = vS.setNum(vFValue);
    ui->lbAcsX->setText(vS1);

    vLSB = aValue[2];
    vMSB = aValue[3];
    vValue = vLSB + (vMSB << 8);
    vFValue = (vValue * 1.0)/(32968/2);
    vS1 = vS.setNum(vFValue);
    ui->lbAcsY->setText(vS1);

    vLSB = aValue[4];
    vMSB = aValue[5];
    vValue = vLSB + (vMSB << 8);
    vFValue = (vValue * 1.0)/(32968/2);
    vS1 = vS.setNum(vFValue);
    ui->lbAcsZ->setText(vS1);

    vLSB = aValue[6];
    vMSB = aValue[7];
    vValue = vLSB + (vMSB << 8);
    vFValue = (vValue * 1.0)/(65526/500);
    vS1 = vS.setNum(vFValue);
    ui->lbGyroX->setText(vS1);

    vLSB = aValue[8];
    vMSB = aValue[9];
    vValue = vLSB + (vMSB << 8);
    vFValue = (vValue * 1.0)/(65526/500);
    vS1 = vS.setNum(vFValue);
    ui->lbGyroY->setText(vS1);

    vLSB = aValue[10];
    vMSB = aValue[11];
    vValue = vLSB + (vMSB << 8);
    vFValue = (vValue * 1.0)/(65526/500);
    vS1 = vS.setNum(vFValue);
    ui->lbGyroZ->setText(vS1);

    vLSB = aValue[12];
    vMSB = aValue[13];
    vValue = vLSB + (vMSB << 8);
    vFValue = (vValue * 1.0);//(32968/2);
    vS1 = vS.setNum(vFValue);
    ui->lbMagX->setText(vS1);

    vLSB = aValue[14];
    vMSB = aValue[15];
    vValue = vLSB + (vMSB << 8);
    vFValue = (vValue * 1.0);//(32968/2);
    vS1 = vS.setNum(vFValue);
    ui->lbMagY->setText(vS1);

    vLSB = aValue[16];
    vMSB = aValue[17];
    vValue = vLSB + (vMSB << 8);
    vFValue = (vValue * 1.0);//(32968/2);
    vS1 = vS.setNum(vFValue);
    ui->lbMagZ->setText(vS1);

}

void Widget::fCharacteristicReadLux(QLowEnergyCharacteristic blechar, QByteArray aValue)
{
    QString vS, vS1;
    vLSB = aValue[0];
    vMSB = aValue[1];
    vValue = vLSB + (vMSB << 8);
    uint16_t vE,vM;
    vM = vValue & 0x0FFF;
    vE = (vValue & 0xF000) >> 12;
    vFValue = vM * (0.01 * pow(2.0,vE));
    vS1 = vS.setNum(vFValue);
    ui->lbLux->setText(vS1);
}
