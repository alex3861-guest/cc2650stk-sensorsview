/*
This module is a part of cc2650stk-sensorsview programm.
cc2650stk-sensorsview is a small app to read all available
sensors from TI cc2650stk sensortag device with default firmware.

Copyright (C) <2018>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QBluetoothDeviceDiscoveryAgent>
#include <QBluetoothDeviceInfo>
#include <QLowEnergyController>
#include <QBluetoothUuid>
#include <stdint.h>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void on_btBluetoothScan_clicked();

    void on_btConnectToDevice_clicked();

//device discovery slots-------------------------------
    void    fAddDevice(const QBluetoothDeviceInfo &device);
    void    fDeviceScanError(QBluetoothDeviceDiscoveryAgent::Error error);
    void    fScanFinished();


    void fDeviceConnected();
    void fServiceDiscovered(QBluetoothUuid vGatt);
    void fServiceScanFinished();
    void fControllerError(QLowEnergyController::Error error);
    void fDeviceDisconnected();
    void fServiceError(QLowEnergyService::ServiceError error);
    void fServiceStateChangedTemp(QLowEnergyService::ServiceState vState);
    void fServiceStateChangedHum(QLowEnergyService::ServiceState vState);
    void fServiceStateChangedBar(QLowEnergyService::ServiceState vState);
    void fServiceStateChangedMov(QLowEnergyService::ServiceState vState);
    void fServiceStateChangedLux(QLowEnergyService::ServiceState vState);

    void fCharacteristicReadTemp(QLowEnergyCharacteristic blechar, QByteArray aValue);
    void fCharacteristicReadHum(QLowEnergyCharacteristic blechar, QByteArray aValue);
    void fCharacteristicReadBar(QLowEnergyCharacteristic blechar, QByteArray aValue);
    void fCharacteristicReadMov(QLowEnergyCharacteristic blechar, QByteArray aValue);
    void fCharacteristicReadLux(QLowEnergyCharacteristic blechar, QByteArray aValue);
private:
    Ui::Widget *ui;
//device discovery -----------------------------------------
    QBluetoothDeviceDiscoveryAgent *oBlutoothDevDiscAgent;
    struct stDevice
    {
        QString name;
        QBluetoothAddress address;
    };
    QList<stDevice> lDevList;
//Low energy controller -------------------------------------
    QLowEnergyController    *oBleController;
    QList<QBluetoothUuid>   lServicesList;

//BLE service -------------------------------
    QLowEnergyService               *oBleServiceTemp;
    QLowEnergyService               *oBleServiceHum;
    QLowEnergyService               *oBleServiceBar;
    QLowEnergyService               *oBleServiceMov;
    QLowEnergyService               *oBleServiceLux;
    QList<QLowEnergyCharacteristic> lCharacteristicsTemp;
    QList<QLowEnergyCharacteristic> lCharacteristicsHum;
    QList<QLowEnergyCharacteristic> lCharacteristicsBar;
    QList<QLowEnergyCharacteristic> lCharacteristicsMov;
    QList<QLowEnergyCharacteristic> lCharacteristicsLux;
    QList<QLowEnergyDescriptor>     lDescriptors;
    QLowEnergyDescriptor            vDescriptorTemp;
    QLowEnergyDescriptor            vDescriptorHum;
    QLowEnergyDescriptor            vDescriptorBar;
    QLowEnergyDescriptor            vDescriptorMov;
    QLowEnergyDescriptor            vDescriptorLux;
    QByteArray                      aValue;
    QByteArray                      aStartMeasurement;
    QByteArray                      aStopMeasurement;
    QByteArray                      aStartNotifications;
    QByteArray                      aStopNotifications;
//    char                            vStartMeasurement = 1;
//    char                            vStopMeasurement = 0;
    u_int8_t                         vLSB;
    u_int8_t                         vMSB;
    u_int8_t                         vEXT;
    int_least16_t                   vValue;
    float                           vFValue;
    const   float                   vScaleLSB = 0.03125;
    void                            fConnectToServices();
};

#endif // WIDGET_H
